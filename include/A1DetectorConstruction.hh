/*
 * Apron 1.0
 * Copyright (c) 2019
 * Servicio Geológico Colombiano / Universidad Distrital Francisco José de Caldas
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla / Alejandra Morales
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef A1DetectorConstruction_h
#define A1DetectorConstruction_h 1

// Geant4 Headers
#include "G4VUserDetectorConstruction.hh"
#include "A1DetectorMessenger.hh"

/// Detector construction class to define materials and geometry.

class G4Tubs;
class G4Box;

class A1DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    A1DetectorConstruction();
    virtual ~A1DetectorConstruction();

    virtual G4VPhysicalVolume* Construct();
    void SetupGeometry(G4LogicalVolume* motherVolume);
    G4LogicalVolume* GetScoringVolume() const { return fScoringVolume; }
    void SetApronThickness(G4double aThickness);
    G4double GetApronThickness() const { return fApronThickness; }
    void SetSourceOrb(G4bool aFlag);
    G4double GetSourceOrb() const { return fSourceOrbFlag; }

  private:
    G4LogicalVolume*		fScoringVolume;
    G4VPhysicalVolume*		fFrontLayer_phys;
    G4VPhysicalVolume*		fBackLayer_phys;
    G4VPhysicalVolume*		fTopTub_phys;
    G4Tubs*					fTopTub1_geo;
    G4Tubs*					fTopTub2_geo;
    G4VPhysicalVolume*		fLeftLayer_phys;
    G4Box*					fLeftLayer1_geo;
    G4Tubs*					fLeftTub1_geo;
    G4VPhysicalVolume*		fRightLayer_phys;
    G4VPhysicalVolume*		fPhantomOrb1_phys;
    G4VPhysicalVolume*		fPhantomOrb2_phys;
    G4VPhysicalVolume*		fSourceOrb_phys;
    G4bool					fSourceOrbFlag;
    G4UImessenger* 	 		fA1DetectorMessenger;
    G4double				fApronThickness;

};

#endif // A1DetectorConstruction_h
