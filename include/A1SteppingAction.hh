/*
 * Apron 1.0
 * Copyright (c) 2019
 * Servicio Geológico Colombiano / Universidad Distrital Francisco José de Caldas
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla / Alejandra Morales
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef A1SteppingAction_h
#define A1SteppingAction_h 1

// C++ Headers
#include <vector>

// Geant4 Headers
#include "G4UserSteppingAction.hh"
#include "globals.hh"
#include "G4VPhysicalVolume.hh"

using namespace std;

class G4LogicalVolume;
class A1EventAction;

/// Stepping action class
///

class A1SteppingAction : public G4UserSteppingAction
{
  public:
    A1SteppingAction(A1EventAction* eventAction);
    virtual ~A1SteppingAction();

    // method from the base class
    virtual void UserSteppingAction(const G4Step*);

  private:
    G4LogicalVolume* 	fScoringVolume;
    A1EventAction*		fEventAction;

};

#endif
