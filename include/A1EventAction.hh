/*
 * NanoParticle 1.0
 * Copyright (c) 2017 Escuela Colombiana de Carreras Industriales - ECCI
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef S1EventAction_h
#define S1EventAction_h 1

// Geant4 Headers
#include "G4UserEventAction.hh"
#include "globals.hh"
#include "G4Run.hh"
#include "G4Event.hh"
#include "G4SystemOfUnits.hh"
#include "G4THitsMap.hh"


class A1RunAction;

using namespace std;

/// Event action class
///

class A1EventAction : public G4UserEventAction
{
  public:
    A1EventAction(A1RunAction* runAction);
    virtual ~A1EventAction();

    virtual void BeginOfEventAction(const G4Event* event);
    virtual void EndOfEventAction(const G4Event* event);

    void AddEdepSO1(G4double Edep) { fEdepSO1 += Edep; }
    void AddEdepSO2(G4double Edep) { fEdepSO2 += Edep; }

  private:
    A1RunAction* 			fRunAction;
    G4double     			fEdepSO1;
    G4double     			fEdepSO2;

};

#endif

    
