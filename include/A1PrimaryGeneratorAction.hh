/*
 * Apron 1.0
 * Copyright (c) 2019
 * Servicio Geológico Colombiano / Universidad Distrital Francisco José de Caldas
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla / Alejandra Morales
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef A1PrimaryGeneratorAction_h
#define A1PrimaryGeneratorAction_h 1

// Geant4 Headers
#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4GeneralParticleSource.hh"
#include "globals.hh"

class G4Event;
class G4Box;

class A1PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    A1PrimaryGeneratorAction();
    virtual ~A1PrimaryGeneratorAction();

    // method from the base class
    virtual void GeneratePrimaries(G4Event*);

    inline const G4GeneralParticleSource* GetParticleSource() const {return fParticleSource; } ;

  private:
        G4GeneralParticleSource* fParticleSource;
};


#endif

