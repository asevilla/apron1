/*
 * Apron 1.0
 * Copyright (c) 2019
 * Servicio Geológico Colombiano / Universidad Distrital Francisco José de Caldas
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla / Alejandra Morales
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef A1ActionInitialization_h
#define A1ActionInitialization_h 1

#include "G4VUserActionInitialization.hh"

/// Action initialization class.

class A1ActionInitialization : public G4VUserActionInitialization
{
  public:
    A1ActionInitialization();
    virtual ~A1ActionInitialization();

    virtual void BuildForMaster() const;
    virtual void Build() const;
};

#endif //A1ActionInitialization_h

    
