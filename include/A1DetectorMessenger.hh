/*
 * Apron 1.0
 * Copyright (c) 2019
 * Servicio Geológico Colombiano / Universidad Distrital Francisco José de Caldas
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla / Alejandra Morales
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia
 *
 * NanoParticle 1.0
 * Copyright (c) 2017 Escuela Colombiana de Carreras Industriales - ECCI
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef A1DetectorMessenger_hh
#define A1DetectorMessenger_hh 1

// C++ Headers
#include <vector>
#include <map>

// Geant4 Headers
#include "G4UImessenger.hh"
#include "globals.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

// A1 Headers
#include "A1DetectorConstruction.hh"

using namespace std;

typedef map <G4String,G4UIdirectory*>         			UIDirectoryCollection ;
typedef map <G4String,G4UIcmdWithADoubleAndUnit*>    	UIcmdWithADoubleAndUnitCollection ;
typedef map <G4String,G4UIcmdWithABool*>    			UIcmdWithABoolCollection ;


class A1DetectorConstruction ;
class G4UIdirectory ;
class G4UIcmdWithADoubleAndUnit ;

class A1DetectorMessenger: public G4UImessenger{

public:

	A1DetectorMessenger(A1DetectorConstruction* aA1DetectorConstruction ) ;
	~A1DetectorMessenger() ;

	void SetNewValue(G4UIcommand* command, G4String aValue) ;

private:

	A1DetectorConstruction*					fA1DetectorConstruction ;

	UIDirectoryCollection					fUIDirectoryCollection ;
	UIcmdWithADoubleAndUnitCollection		fUIcmdWithADoubleAndUnitCollection;
	UIcmdWithABoolCollection				fUIcmdWithABoolCollection;

} ;

#endif // A1DetectorMessenger_hh
