/*
 * Apron 1.0
 * Copyright (c) 2019
 * Servicio Geológico Colombiano / Universidad Distrital Francisco José de Caldas
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla / Alejandra Morales
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia
 *
 * NanoParticle 1.0
 * Copyright (c) 2017 Escuela Colombiana de Carreras Industriales - ECCI
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef A1RunAction_h
#define A1RunAction_h 1

// Geant4 Headers
#include "G4UserRunAction.hh"
#include "G4Accumulable.hh"
#include "globals.hh"

// A1 Headers
#include "A1DetectorConstruction.hh"


using namespace std;

/// Run action class
///

class A1RunAction : public G4UserRunAction
{

  public:
    A1RunAction();
    virtual ~A1RunAction();

    virtual void BeginOfRunAction(const G4Run*);
    virtual void EndOfRunAction(const G4Run*);

    void CreateHistos();
    void CreateNTuples();

    inline void AddEdepSO1(G4double edep) {fEdepSO1+=edep;}
    inline void AddEdepSO2(G4double edep) {fEdepSO2+=edep;}

private:

    G4Accumulable<G4double>     	fEdepSO1;
    G4Accumulable<G4double>	   		fEdepSO2;

};

#endif // A1RunAction_h

