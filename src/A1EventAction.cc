/*
 * NanoParticle 1.0
 * Copyright (c) 2017 Escuela Colombiana de Carreras Industriales - ECCI
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

// Geant4 Headers
#include "G4RunManager.hh"
#include "G4EventManager.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"

// A1 Headers
#include "A1EventAction.hh"
#include "A1Analysis.hh"
#include "A1RunAction.hh"

A1EventAction::A1EventAction(A1RunAction* runAction):
G4UserEventAction(),
fRunAction(runAction),
fEdepSO1(0.),
fEdepSO2(0.)
{}

A1EventAction::~A1EventAction()
{}

void A1EventAction::BeginOfEventAction(const G4Event* anEvent)
{    
	G4int nEvents = G4RunManager::GetRunManager()->GetCurrentRun()->GetNumberOfEventToBeProcessed() ;
	if(nEvents>10){
		G4int 	fraction 	= G4int(nEvents/100) ;
		if(anEvent->GetEventID()%fraction == 0)
			G4cout<<"("<<(anEvent->GetEventID()/(nEvents*1.0)*100)<<" %)"<<G4endl ;

	}else {
		G4int 	fraction 	= G4int(nEvents/1) ;
		if(anEvent->GetEventID()%fraction == 0)
			G4cout<<"("<<(anEvent->GetEventID()/(nEvents*1.0)*100)<<" %)"<<G4endl ;
	}

	fEdepSO1=0.;
	fEdepSO2=0.;

}

void A1EventAction::EndOfEventAction(const G4Event* anEvent)
{   

	if(!(fEdepSO1>0. || fEdepSO2>0.)) return;

	// Analysis manager
	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

	// accumulate statistics in run action
	// Analysis manager

	fRunAction->AddEdepSO1(fEdepSO1);
	fRunAction->AddEdepSO2(fEdepSO2);

	if(fEdepSO1>0.)
		analysisManager->FillH1(0,fEdepSO1);
	if(fEdepSO2>0.)
		analysisManager->FillH1(1,fEdepSO2);


}
