/*
 * Apron 1.0
 * Copyright (c) 2019
 * Servicio Geológico Colombiano / Universidad Distrital Francisco José de Caldas
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla / Alejandra Morales
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

// Geant4 Headers
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

// A1 Headers
#include "A1DetectorConstruction.hh"
#include "A1EventAction.hh"
#include "A1SteppingAction.hh"
#include "G4VPhysicalVolume.hh"
#include "A1Analysis.hh"

using namespace std;

A1SteppingAction::A1SteppingAction(A1EventAction* eventAction)
: G4UserSteppingAction(),
  fScoringVolume(0)
{
	fEventAction = eventAction;
}

A1SteppingAction::~A1SteppingAction()
{}

void A1SteppingAction::UserSteppingAction(const G4Step* aStep)
{

	if (!fScoringVolume) {
		const A1DetectorConstruction* detectorConstruction
		= static_cast<const A1DetectorConstruction*>
		(G4RunManager::GetRunManager()->GetUserDetectorConstruction());
		fScoringVolume = detectorConstruction->GetScoringVolume();
	}

	G4String volumeName = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetLogicalVolume()->GetName();

	//if(aStep->GetTrack()->GetTrackID()==1)
	//G4cout<<aStep->GetTrack()->GetVertexKineticEnergy()<<G4endl;

	if (volumeName!="sensitiveOrb_log") return;

	G4double TotalEnergyDeposit = aStep->GetTotalEnergyDeposit();

	if (TotalEnergyDeposit==0) return;

	G4int CopyNo = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume(1)->GetCopyNo();

	if (CopyNo==1)
		fEventAction->AddEdepSO1(aStep->GetTotalEnergyDeposit());

	if (CopyNo==2)
		fEventAction->AddEdepSO2(aStep->GetTotalEnergyDeposit());

}

