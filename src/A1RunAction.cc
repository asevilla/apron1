/*
 * Apron 1.0
 * Copyright (c) 2019
 * Servicio Geológico Colombiano / Universidad Distrital Francisco José de Caldas
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla / Alejandra Morales
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */
// Geant4 Headers
#include "G4RunManager.hh"
#include "G4Run.hh"
#include "G4AccumulableManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

// ROOT Headers
#include <TH1.h>

// A1 Headers
#include "A1PrimaryGeneratorAction.hh"
#include "A1RunAction.hh"
#include "A1Analysis.hh"

class G4AccumulableManager;

using namespace std;

A1RunAction::A1RunAction(): G4UserRunAction(),
		fEdepSO1(0.),
		fEdepSO2(0.)
{ 

	G4AccumulableManager* accumulableManager = G4AccumulableManager::Instance();
	// Register accumulable to the accumulable manager
	accumulableManager->RegisterAccumulable(fEdepSO1);
	accumulableManager->RegisterAccumulable(fEdepSO2);

	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
	G4cout << "Using " << analysisManager->GetType() << G4endl;
	analysisManager->SetVerboseLevel(0);
	analysisManager->SetFileName("data/NP1");

	// Create n-Tuples and Histos
	CreateNTuples();
	CreateHistos();

}

A1RunAction::~A1RunAction()
{

	delete G4AccumulableManager::Instance();
	delete G4AnalysisManager::Instance();
}

void A1RunAction::BeginOfRunAction(const G4Run* aRun)
{ 

	//inform the runManager to save random number seed
	G4RunManager::GetRunManager()->SetRandomNumberStore(false);

	// reset accumulables to their initial values
	G4AccumulableManager* accumulableManager = G4AccumulableManager::Instance();
	accumulableManager->Reset();

	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

	// Open an output file
	analysisManager->OpenFile();

}

void A1RunAction::EndOfRunAction(const G4Run* aRun)
{
	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
	// Save histograms and ntuples and close output file
	analysisManager->Write();
	analysisManager->CloseFile();

	G4int nofEvents = aRun->GetNumberOfEvent();
	if (nofEvents == 0) return;

	//Equivalent Dose
	new G4UnitDefinition("milisievert","uSv","Equivalent Dose",gray/1e6);

	const A1DetectorConstruction* detectorConstruction =
			static_cast<const A1DetectorConstruction*>(G4RunManager::GetRunManager()->GetUserDetectorConstruction());
	G4LogicalVolume* scoringVolume = detectorConstruction->GetScoringVolume();


	G4double mass = scoringVolume->GetMass();

	//Edep Values
	G4double EdepSO1 = fEdepSO1.GetValue();
	G4double EdepSO2 = fEdepSO2.GetValue();

	//Dose Values
	G4double DoseSO1 = EdepSO1/mass;
	G4double DoseSO2 = EdepSO2/mass;

	// Merge accumulables
	G4AccumulableManager* accumulableManager = G4AccumulableManager::Instance();
	accumulableManager->Merge();

	// Print
	//
	if (IsMaster()) {
		G4cout
		<< G4endl
		<< "--------------------End of Global Run-----------------------"
		<< G4endl
		<< " Cumulated Hp(10) per inicident photon, in sensitive orb 1 : "<< G4BestUnit(DoseSO1/nofEvents,"Equivalent Dose")
		<< G4endl
		<< " Cumulated Hp(10) per inicident photon, in sensitive orb 2 : "<< G4BestUnit(DoseSO2/nofEvents,"Equivalent Dose")
		<< G4endl;
		if(DoseSO1>0.0){
			G4cout<< " Apron relative efficiency [%]: "<< (DoseSO1-DoseSO2)/DoseSO1*100
			<< G4endl;
		}
	}

}

void A1RunAction::CreateNTuples(){

	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
	analysisManager->SetNtupleMerging(true);

	// Creating ntuples
	analysisManager->SetFirstNtupleId(0);
}

void A1RunAction::CreateHistos(){

	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

	// Creating histos
	analysisManager->SetFirstHistoId(0);

	// id = 0
	analysisManager->CreateH1("EnergyDepositSO1","energy deposit in sensitive orb 1", 6000, 0., 600*keV,"keV");
	analysisManager->SetH1Activation(0,false);

	// id = 1
	analysisManager->CreateH1("EnergyDepositSO2","energy deposit in sensitive orb 2", 6000, 0., 600*keV,"keV");
	analysisManager->SetH1Activation(1,false);

}

