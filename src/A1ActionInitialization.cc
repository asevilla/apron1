/*
 * Apron 1.0
 * Copyright (c) 2019
 * Servicio Geológico Colombiano / Universidad Distrital Francisco José de Caldas
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla / Alejandra Morales
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */
#include "A1ActionInitialization.hh"
#include "A1PrimaryGeneratorAction.hh"
#include "A1RunAction.hh"
#include "A1EventAction.hh"
#include "A1SteppingAction.hh"

A1ActionInitialization::A1ActionInitialization()
 : G4VUserActionInitialization()
{}

A1ActionInitialization::~A1ActionInitialization()
{}

void A1ActionInitialization::BuildForMaster() const
{
	A1RunAction* runAction = new A1RunAction();
	SetUserAction(runAction);
	}

void A1ActionInitialization::Build() const
{
  SetUserAction(new A1PrimaryGeneratorAction);
  A1RunAction* runAction = new A1RunAction();
  SetUserAction(runAction);
  A1EventAction* eventAction = new A1EventAction(runAction);
  SetUserAction(eventAction);
  SetUserAction(new A1SteppingAction(eventAction));


}  
