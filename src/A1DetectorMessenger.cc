/*
 * Apron 1.0
 * Copyright (c) 2019
 * Servicio Geológico Colombiano / Universidad Distrital Francisco José de Caldas
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla / Alejandra Morales
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia
 *
 * NanoParticle 1.0
 * Copyright (c) 2017 Escuela Colombiana de Carreras Industriales - ECCI
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

// A1 Headers
#include "A1DetectorMessenger.hh"

A1DetectorMessenger::A1DetectorMessenger(A1DetectorConstruction* aA1DetectorConstruction){

	fA1DetectorConstruction = aA1DetectorConstruction ;

	fUIDirectoryCollection["A1Geometry"] = new G4UIdirectory("/A1/GeometryConstruction/") ;
	fUIDirectoryCollection["A1Geometry"] -> SetGuidance("Geometry construction commands") ;

	fUIcmdWithADoubleAndUnitCollection["SetApronThickness"] = new G4UIcmdWithADoubleAndUnit("/A1/GeometryConstruction/SetApronThickness",this) ;
	fUIcmdWithADoubleAndUnitCollection["SetApronThickness"] -> SetGuidance("Set apron thickness");
	fUIcmdWithADoubleAndUnitCollection["SetApronThickness"] -> SetParameterName("Thickness",false);
	fUIcmdWithADoubleAndUnitCollection["SetApronThickness"] -> SetUnitCategory("Length");
	fUIcmdWithADoubleAndUnitCollection["SetApronThickness"] -> AvailableForStates(G4State_PreInit, G4State_Idle);

	fUIcmdWithABoolCollection["SetSourceOrb"] = new G4UIcmdWithABool("/A1/GeometryConstruction/SetSourceOrb",this) ;
	fUIcmdWithABoolCollection["SetSourceOrb"] -> SetGuidance("Set a flag for source orb switch on/off");
	fUIcmdWithABoolCollection["SetSourceOrb"] -> SetParameterName("aFlag",false);
	fUIcmdWithABoolCollection["SetSourceOrb"] -> AvailableForStates(G4State_PreInit, G4State_Idle);

}


A1DetectorMessenger::~A1DetectorMessenger(){

	// UI Directory Collection
	for(size_t i = 0; i < fUIDirectoryCollection.size(); i++){
		delete fUIDirectoryCollection[i] ;
	}

	// UI cmd With a Double and Unit Collection
	for(size_t i = 0; i < fUIcmdWithADoubleAndUnitCollection.size(); i++){
		delete fUIcmdWithADoubleAndUnitCollection[i] ;
	}

	// UI cmd With a Bool Collection
	for(size_t i = 0; i < fUIcmdWithABoolCollection.size(); i++){
		delete fUIcmdWithABoolCollection[i] ;
	}
}

void A1DetectorMessenger::SetNewValue(G4UIcommand* command,G4String aValue ){

	if( command == fUIcmdWithADoubleAndUnitCollection["SetApronThickness"] ) { fA1DetectorConstruction->SetApronThickness(fUIcmdWithADoubleAndUnitCollection["SetApronThickness"] ->GetNewDoubleValue(aValue)); }


	if( command == fUIcmdWithABoolCollection["SetSourceOrb"] ) { fA1DetectorConstruction->SetSourceOrb(fUIcmdWithABoolCollection["SetApronThickness"]->GetNewBoolValue(aValue)); }

}

