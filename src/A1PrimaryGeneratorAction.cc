/*
 * Apron 1.0
 * Copyright (c) 2019
 * Servicio Geológico Colombiano / Universidad Distrital Francisco José de Caldas
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla / Alejandra Morales
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */


// Geant4 Headers
#include "G4SystemOfUnits.hh"
#include "G4ParticleTable.hh"

// A1 Headers
#include "A1PrimaryGeneratorAction.hh"

A1PrimaryGeneratorAction::A1PrimaryGeneratorAction(): G4VUserPrimaryGeneratorAction()
{

        G4cout<<"01 - Primary Generator action have started !!!"<<G4endl;
        fParticleSource = new G4GeneralParticleSource();
}

A1PrimaryGeneratorAction::~A1PrimaryGeneratorAction()
{
  delete fParticleSource;
}

void A1PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  fParticleSource->GeneratePrimaryVertex(anEvent);

}

