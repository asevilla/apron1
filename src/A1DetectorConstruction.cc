/*
 * Apron 1.0
 * Copyright (c) 2019
 * Servicio Geológico Colombiano / Universidad Distrital Francisco José de Caldas
 * All Right Reserved.
 *
 * Developed by Andrés Camilo Sevilla / Alejandra Morales
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

// Geant4 Headers
#include "G4SystemOfUnits.hh"
#include "G4Box.hh"
#include "G4Colour.hh"
#include "G4LogicalVolume.hh"
#include "G4VisAttributes.hh"
#include "G4PVPlacement.hh"
#include "G4NistManager.hh"
#include "G4Tubs.hh"
#include "G4Orb.hh"
#include "G4SubtractionSolid.hh"
#include "G4GeometryManager.hh"
#include "G4RunManager.hh"

// NP1 Headers
#include "A1DetectorConstruction.hh"

A1DetectorConstruction::A1DetectorConstruction()
: G4VUserDetectorConstruction(),
  fScoringVolume(0),
  fFrontLayer_phys(0),
  fBackLayer_phys(0),
  fTopTub_phys(0),
  fTopTub1_geo(0),
  fTopTub2_geo(0),
  fLeftLayer_phys(0),
  fRightLayer_phys(0),
  fSourceOrbFlag(false),
  fApronThickness(0.5*mm) // Lead Equivalent
{
	fA1DetectorMessenger = new A1DetectorMessenger(this) ;
	fPhantomOrb1_phys=NULL;
	fPhantomOrb2_phys=NULL;
	fLeftLayer1_geo=NULL;
	fLeftTub1_geo=NULL;
	fSourceOrb_phys=NULL;
}

A1DetectorConstruction::~A1DetectorConstruction()
{
	delete fA1DetectorMessenger ;
}

G4VPhysicalVolume* A1DetectorConstruction::Construct()
{  

	// Get nist material manager
	G4NistManager* nist = G4NistManager::Instance();

	// Define materials
	G4Material* AIR = nist->FindOrBuildMaterial("G4_AIR");

	// General Attributes
	G4VisAttributes* simpleInvisibleSVisAtt;
	simpleInvisibleSVisAtt= new G4VisAttributes(G4Colour(0.,0.5,0.5,0.1));
	simpleInvisibleSVisAtt->SetVisibility(false);

	// Geometrical Volume =========================================================================================

	// World
	G4double world_size_X = (10/2.)*m;
	G4VSolid* world_geo = new G4Box("world_geo", world_size_X, world_size_X, world_size_X);

	//Logical Volume ==============================================================================================

	// World
	G4LogicalVolume* world_log = new G4LogicalVolume(world_geo,AIR,"world_log");
	world_log -> SetVisAttributes(simpleInvisibleSVisAtt);

	//Physics Volume  =============================================================================================

	G4VPhysicalVolume* world_phys =
			new G4PVPlacement(0,					//no rotation
					G4ThreeVector(),				//at (0,0,0)
					world_log,						//its logical volume
					"world_phys",					//its name
					0,								//its mother  volume
					false,							//no boolean operation
					0,								//copy number
					0);								//overlaps checking

	SetupGeometry(world_log);

	//always return the physical World
	//
	return world_phys;

}

void A1DetectorConstruction::SetupGeometry(G4LogicalVolume* motherVolume){


	G4RotationMatrix*  RY90 = new G4RotationMatrix();
	RY90->rotateY(90*deg);

	// Get nist material manager
	G4NistManager* nist = G4NistManager::Instance();

	// Define materials
	G4Material* Pb = nist->FindOrBuildMaterial("G4_Pb");
	G4Material* H2O = nist->FindOrBuildMaterial("G4_WATER");

	// Geometrical Volume =========================================================================================

	// Front Layer
	G4double frontLayer_dx=(50./2.)*cm, frontLayer_dy=(160./2.)*cm, frontLayer_dz=fApronThickness/2.;
	G4VSolid* frontLayer_geo = new G4Box("frontLayer_geo", frontLayer_dx, frontLayer_dy, frontLayer_dz);


	// Back Layer
	G4double backLayer_dx=(50./2.)*cm, backLayer_dy=(50./2.)*cm, backLayer_dz=fApronThickness/2.;
	G4VSolid* backLayer_geo = new G4Box("backLayer_geo", backLayer_dx,backLayer_dy, backLayer_dz);

	// Top Tub 1
	G4double topTub1_rmin=(10)*cm, topTub1_rmax=topTub1_rmin+fApronThickness, topTub1_dz=frontLayer_dx;
	fTopTub1_geo = new G4Tubs("topTub1_geo",topTub1_rmin,topTub1_rmax,topTub1_dz,0*deg,180*deg);

	// Top Tub 2
	G4double topTub2_rmax=topTub1_rmin+fApronThickness, topTub2_dz=frontLayer_dx;
	fTopTub2_geo = new G4Tubs("topTub2_geo",0*cm,topTub2_rmax,topTub2_dz,0*deg,360*deg);

	// Top Tub
	G4SubtractionSolid* topTub_geo =
			new G4SubtractionSolid("topTub_geo", fTopTub1_geo, fTopTub2_geo,RY90,G4ThreeVector(0,topTub1_rmax,0));

	// Left layer 1
	G4double leftLayer1_dx=topTub1_rmin, leftLayer1_dy=backLayer_dy, leftLayer1_dz=fApronThickness/2.;
	fLeftLayer1_geo = new G4Box("leftLayer1_geo", leftLayer1_dx, leftLayer1_dy, leftLayer1_dz);

	// Left Tub 1
	G4double leftTub1_rmax=topTub1_rmin, leftTub1_dz=fApronThickness;
	fLeftTub1_geo = new G4Tubs("leftTub1_geo",0*cm,leftTub1_rmax,leftTub1_dz,0*deg,360*deg);

	// Left layer
	G4SubtractionSolid* leftLayer_geo =
			new G4SubtractionSolid("leftLayer_geo", fLeftLayer1_geo, fLeftTub1_geo,0,G4ThreeVector(0,leftLayer1_dy,0));

	// Phantom Orb
	G4double phantomOrb_rmax=(30/2.)*mm ;
	G4VSolid* phantomOrb_geo = new G4Orb("phantomOrb_geo", phantomOrb_rmax);


	// Sensitive Orb
	G4double sensitiveOrb_rmax=(10/2.)*mm ;
	G4VSolid* sensitiveOrb_geo = new G4Orb("sensitiveOrb_geo", sensitiveOrb_rmax);

	// Source Orb
	G4double sorceOrb_rmax=(10/2.)*mm ;
	G4VSolid* sorceOrb_geo = new G4Orb("sorceOrb_geo", sorceOrb_rmax);


	//Logical Volume ==============================================================================================


	G4VisAttributes* ApronVisAtt;
	ApronVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,0.0,1.0));
	ApronVisAtt->SetVisibility(true);

	G4VisAttributes* WaterVisAtt;
	WaterVisAtt= new G4VisAttributes(G4Colour(0.0,1.0,1.0,0.5));
	WaterVisAtt->SetVisibility(true);

	G4VisAttributes* SensitiveVisAtt;
	SensitiveVisAtt= new G4VisAttributes(G4Colour(0.0,0.0,1.0,0.9));
	SensitiveVisAtt->SetVisibility(true);

	// Front Layer
	G4LogicalVolume* frontLayer_log = new G4LogicalVolume(frontLayer_geo,Pb,"frontLayer_log");
	frontLayer_log -> SetVisAttributes(ApronVisAtt);

	// back Layer
	G4LogicalVolume* backLayer_log = new G4LogicalVolume(backLayer_geo,Pb,"backLayer_log");
	backLayer_log -> SetVisAttributes(ApronVisAtt);

	// Top Tub
	G4LogicalVolume* topTub_log = new G4LogicalVolume(topTub_geo,Pb,"topTub_log");
	topTub_log -> SetVisAttributes(ApronVisAtt);

	// Left Layer
	G4LogicalVolume* leftLayer_log = new G4LogicalVolume(leftLayer_geo,Pb,"leftLayer_log");
	leftLayer_log -> SetVisAttributes(ApronVisAtt);

	// Phantom Orb
	G4LogicalVolume* phantomOrb_log = new G4LogicalVolume(phantomOrb_geo,H2O,"phantomOrb_log");
	phantomOrb_log -> SetVisAttributes(WaterVisAtt);

	// Sensitive Orb
	G4LogicalVolume* sensitiveOrb_log = new G4LogicalVolume(sensitiveOrb_geo,H2O,"sensitiveOrb_log");
	sensitiveOrb_log -> SetVisAttributes(SensitiveVisAtt);

	fScoringVolume=sensitiveOrb_log;

	// Sensitive Orb
	G4LogicalVolume* sourceOrb_log = new G4LogicalVolume(sorceOrb_geo,H2O,"sourceOrb_geo");
	sourceOrb_log -> SetVisAttributes(WaterVisAtt);

	//Physics Volume  =============================================================================================

	// Front Layer
	fFrontLayer_phys = new G4PVPlacement(0,															//no rotation
			G4ThreeVector(0,-(frontLayer_dy-backLayer_dy),-fApronThickness/2.),		//at (0,0,0)
			frontLayer_log,															//its logical volume
			"frontLayer_phys", 	 													//its name
			motherVolume, 															//its mother  volume
			false,																	//no boolean operation
			0,																		//copy number
			false);					 												//overlaps checking

	// Back Layer
	fBackLayer_phys = new G4PVPlacement(0,															//no rotation
			G4ThreeVector(0,0,-(2*topTub1_rmin+3*fApronThickness/2.)),				//at (0,0,0)
			backLayer_log,															//its logical volume
			"backLayer_phys", 	 													//its name
			motherVolume, 															//its mother  volume
			false,																	//no boolean operation
			0,																		//copy number
			false);					 												//overlaps checking

	// Top Tub
	fTopTub_phys = new G4PVPlacement(RY90,															//no rotation
			G4ThreeVector(0,frontLayer_dx,-topTub1_rmax),							//at (0,0,0)
			topTub_log,																//its logical volume
			"topTub_phys", 	 														//its name
			motherVolume, 															//its mother  volume
			false,																	//no boolean operation
			0,																		//copy number
			false);					 												//overlaps checking

	// Left Layer
	fLeftLayer_phys = new G4PVPlacement(RY90,															//no rotation
			G4ThreeVector(frontLayer_dx-fApronThickness/2.,0,-(topTub1_rmax)),		//at (0,0,0)
			leftLayer_log,															//its logical volume
			"leftLayer_phys", 	 													//its name
			motherVolume, 															//its mother  volume
			false,																	//no boolean operation
			0,																		//copy number
			false);					 												//overlaps checking

	// Right Layer
	fRightLayer_phys = new G4PVPlacement(RY90,															//no rotation
			G4ThreeVector(-(frontLayer_dx-fApronThickness/2.),0,-(topTub1_rmax)),	//at (0,0,0)
			leftLayer_log,															//its logical volume
			"rightLayer_phys", 	 													//its name
			motherVolume, 															//its mother  volume
			false,																	//no boolean operation
			0,																		//copy number
			false);					 												//overlaps checking

	// Sensitive Orb
	new G4PVPlacement(0,															//no rotation
			G4ThreeVector(),														//at (0,0,0)
			sensitiveOrb_log,														//its logical volume
			"sensitiveOrb_phys", 	 												//its name
			phantomOrb_log, 														//its mother  volume
			false,																	//no boolean operation
			0,																		//copy number
			false);					 												//overlaps checking

	// Phantom Orb 1
	fPhantomOrb1_phys = new G4PVPlacement(0,										//no rotation
			G4ThreeVector(0,0,(phantomOrb_rmax)),									//at (0,0,0)
			phantomOrb_log,															//its logical volume
			"phantomOrb_phys", 	 													//its name
			motherVolume, 															//its mother  volume
			false,																	//no boolean operation
			1,																		//copy number
			false);					 												//overlaps checking

	// Phantom Orb 2
	fPhantomOrb2_phys =new G4PVPlacement(0,											//no rotation
			G4ThreeVector(0,0,-(phantomOrb_rmax+fApronThickness)),					//at (0,0,0)
			phantomOrb_log,															//its logical volume
			"phantomOrb_phys", 	 													//its name
			motherVolume, 															//its mother  volume
			false,																	//no boolean operation
			2,																		//copy number
			false);					 												//overlaps checking

	if(fSourceOrbFlag){
		// Source Orb
		fSourceOrb_phys =new G4PVPlacement(0,											//no rotation
				G4ThreeVector(0,0,100*cm),												//at (0,0,0)
				sourceOrb_log,															//its logical volume
				"sourceOrb_phys", 	 													//its name
				motherVolume, 															//its mother  volume
				false,																	//no boolean operation
				0,																		//copy number
				false);					 												//overlaps checking
		}

}

void A1DetectorConstruction::SetApronThickness(G4double aThickness){

	G4RotationMatrix*  RY90 = new G4RotationMatrix();
	RY90->rotateY(90*deg);

	G4GeometryManager* geoman = G4GeometryManager::GetInstance() ;

	fApronThickness=aThickness;

	// Front Layer
	geoman->OpenGeometry(fFrontLayer_phys);
	G4Box* frontLayer_geo = static_cast<G4Box*>(this->fFrontLayer_phys->GetLogicalVolume()->GetSolid());
	frontLayer_geo->SetZHalfLength(fApronThickness/2.);
	fFrontLayer_phys->SetTranslation(G4ThreeVector(fFrontLayer_phys->GetTranslation().x(),fFrontLayer_phys->GetTranslation().y(),-fApronThickness/2.));
	geoman->CloseGeometry(fFrontLayer_phys);

	// Back Layer
	geoman->OpenGeometry(fBackLayer_phys);
	G4Box* backLayer_geo = static_cast<G4Box*>(this->fBackLayer_phys->GetLogicalVolume()->GetSolid());
	backLayer_geo->SetZHalfLength(fApronThickness/2.);
	fBackLayer_phys->SetTranslation(G4ThreeVector(fBackLayer_phys->GetTranslation().x(),fBackLayer_phys->GetTranslation().y(),-(2*fTopTub1_geo->GetInnerRadius()+3*fApronThickness/2.)));
	geoman->CloseGeometry(fBackLayer_phys);

	// Top Tub
	geoman->OpenGeometry(fTopTub_phys);
	fTopTub1_geo->SetOuterRadius(fTopTub1_geo->GetInnerRadius()+fApronThickness);
	this->fTopTub_phys->GetLogicalVolume()->SetSolid(new G4SubtractionSolid("topTub_geo", fTopTub1_geo, fTopTub2_geo,RY90,G4ThreeVector(0,fTopTub1_geo->GetOuterRadius(),0)));
	fTopTub_phys->SetTranslation(G4ThreeVector(fTopTub_phys->GetTranslation().x(),fTopTub_phys->GetTranslation().y(),-fTopTub1_geo->GetOuterRadius()));
	geoman->CloseGeometry(fTopTub_phys);

	// Left Layer
	geoman->OpenGeometry(fLeftLayer_phys);
	fLeftLayer_phys->SetTranslation(G4ThreeVector(fLeftLayer_phys->GetTranslation().x(),fLeftLayer_phys->GetTranslation().y(),-fTopTub1_geo->GetOuterRadius()));
	geoman->CloseGeometry(fLeftLayer_phys);

	// Right Layer
	geoman->OpenGeometry(fRightLayer_phys);
	fRightLayer_phys->SetTranslation(G4ThreeVector(fRightLayer_phys->GetTranslation().x(),fLeftLayer_phys->GetTranslation().y(),-fTopTub1_geo->GetOuterRadius()));
	geoman->CloseGeometry(fRightLayer_phys);

	// Phantom Orb 2
	geoman->OpenGeometry(fPhantomOrb2_phys);
	G4Orb* phantomOrb_geo = static_cast<G4Orb*>(this->fPhantomOrb2_phys->GetLogicalVolume()->GetSolid());
	fPhantomOrb2_phys->SetTranslation(G4ThreeVector(fPhantomOrb2_phys->GetTranslation().x(),fPhantomOrb2_phys->GetTranslation().y(),-(phantomOrb_geo->GetRadius()+fApronThickness)));
	geoman->CloseGeometry(fPhantomOrb2_phys);

	G4RunManager::GetRunManager()->ReinitializeGeometry();

}

void A1DetectorConstruction::SetSourceOrb(G4bool aFlag){

	G4RunManager::GetRunManager()->ReinitializeGeometry();
	fSourceOrbFlag=aFlag;

}


